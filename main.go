package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/charmbracelet/bubbles/stopwatch"
	tea "github.com/charmbracelet/bubbletea"
)

type model struct {
	cursorX   int64
	cursorY   int64
	game      *game
	stopwatch stopwatch.Model
	time      time.Time
}

func initialModel() model {
	game, _ := newGame(20, 10, 35)
	m := model{
		game:      game,
		stopwatch: stopwatch.NewWithInterval(time.Millisecond),
	}
	return m
}

func (m model) Init() tea.Cmd {
	return nil
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {

	// Is it a key press?
	case tea.KeyMsg:

		// Cool, what was the actual key pressed?
		switch msg.String() {

		// These keys should exit the program.
		case "ctrl+c", "q":
			return m, tea.Quit

		case "up":
			if m.cursorY > 0 {
				m.cursorY--
			}

		case "down":
			if m.cursorY < m.game.height-1 {
				m.cursorY++
			}

		case "left":
			if m.cursorX > 0 {
				m.cursorX--
			}

		case "right":
			if m.cursorX < m.game.width-1 {
				m.cursorX++
			}

		case "enter":
			m.game.discover(m.cursorX, m.cursorY)
			if m.game.status == STATUS_LOST {
				if m.stopwatch.Running() {
					return m, m.stopwatch.Stop()
				}
				return m, nil
			}
			m.game.judgeWin()
			if m.game.status == STATUS_WON {
				if m.stopwatch.Running() {
					return m, m.stopwatch.Stop()
				}
				return m, nil
			}
			if !m.stopwatch.Running() {
				m.time = time.Now()
				return m, m.stopwatch.Start()
			}
		case " ":
			m.game.setFlag(m.cursorX, m.cursorY)
		}
	}

	var cmd tea.Cmd
	m.stopwatch, cmd = m.stopwatch.Update(msg)
	return m, cmd
}

func (m model) View() string {
	display, _ := m.game.getDisplay()
	if m.stopwatch.Elapsed().Milliseconds()%200 < 100 || m.game.status != STATUS_ON {
		display[m.cursorY][m.cursorX] = '#'
	}
	s := fmt.Sprintf("Flags:%d  ", m.game.flags)
	var elapsedTime int64 = 0
	if m.time.Unix() > 0 {
		elapsedTime = time.Now().UnixMilli() - m.time.UnixMilli()
	}
	s += fmt.Sprintf("Time:%dms\n", elapsedTime)
	for i := int64(0); i < m.game.height; i++ {
		s += string(display[i]) + "\n"
	}

	s = strings.ReplaceAll(s, string(FLAG), "\033[31m"+string(FLAG)+"\033[0m")

	if m.game.status == STATUS_WON {
		s += "YOU WIN! \nPress Q to quit the game.\n"
	}
	if m.game.status == STATUS_LOST {
		s += "YOU LOSE!\nPress Q to quit the game.\n"
	}

	return s
}

func main() {
	p := tea.NewProgram(initialModel())
	if err := p.Start(); err != nil {
		fmt.Printf("Alas, there's been an error: %v", err)
		os.Exit(1)
	}
}
