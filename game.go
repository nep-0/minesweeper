package main

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

type game struct {
	width     int64
	height    int64
	mines     int64
	board     [][]byte
	progress  [][]progress
	status    status
	remaining int64 // number of remaining undiscovered blocks
	flags     int64 // number of mines - number of flags
}

const (
	MINE    = 'X'
	EMPTY   = ' '
	FLAG    = 'P'
	UNKNOWN = '?'
)

type progress uint64

const (
	PROGRESS_UNKNOWN = iota
	PROGRESS_DISCOVERED
	PROGRESS_FLAG
)

type status uint64

const (
	STATUS_ON = iota
	STATUS_WON
	STATUS_LOST
)

func newGame(width, height, mines int64) (*game, error) {
	if mines > width*height {
		return &game{}, errors.New("too many mines")
	}

	board := make([][]byte, height)
	for i := int64(0); i < height; i++ {
		board[i] = make([]byte, width)
	}

	progressSlice := make([][]progress, height)
	for i := int64(0); i < height; i++ {
		progressSlice[i] = make([]progress, width)
	}

	rand1 := rand.New(rand.NewSource(time.Now().UnixNano()))

	for i := int64(0); i < mines; i++ {
		x := rand1.Int63n(width)
		y := rand1.Int63n(height)

		for board[y][x] == MINE { // the place is already taken by another mine
			x = rand1.Int63n(width)
			y = rand1.Int63n(height)
		}

		board[y][x] = MINE
	}

	g := &game{
		width:     width,
		height:    height,
		mines:     mines,
		board:     board,
		progress:  progressSlice,
		status:    STATUS_ON,
		remaining: width * height,
		flags:     mines,
	}

	for i := int64(0); i < width; i++ {
		for j := int64(0); j < height; j++ {
			if g.board[j][i] != MINE {
				nearby, err := g.getNearby(i, j)
				if err != nil {
					panic(err)
				}
				if nearby != 0 {
					g.board[j][i] = []byte(fmt.Sprint(nearby))[0]
				} else {
					g.board[j][i] = EMPTY
				}
			}
		}
	}

	return g, nil
}

func (g *game) getDisplay() ([][]byte, error) {
	s := make([][]byte, g.height)
	for i := int64(0); i < g.height; i++ {
		s[i] = make([]byte, g.width)
	}
	for i := int64(0); i < g.width; i++ {
		for j := int64(0); j < g.height; j++ {
			switch g.progress[j][i] {
			case PROGRESS_UNKNOWN:
				s[j][i] = UNKNOWN
			case PROGRESS_FLAG:
				s[j][i] = FLAG
			case PROGRESS_DISCOVERED:
				s[j][i] = g.board[j][i]
			default:
				return s, errors.New("unexpected progress")
			}
		}
	}
	return s, nil
}

func (g *game) discover(x, y int64) error {
	if x < 0 || y < 0 || x >= g.width || y >= g.height {
		return errors.New("no such position")
	}

	if g.progress[y][x] != PROGRESS_UNKNOWN {
		return errors.New("nothing to discover")
	}
	if g.board[y][x] == MINE {
		g.progress[y][x] = PROGRESS_DISCOVERED
		g.status = STATUS_LOST
		return nil
	}

	nearby, err := g.getNearby(x, y)
	if err != nil {
		return err
	}

	g.progress[y][x] = PROGRESS_DISCOVERED
	g.remaining--
	if nearby == 0 {
		g.discover(x+1, y)
		g.discover(x-1, y)
		g.discover(x, y+1)
		g.discover(x, y-1)
		g.discover(x+1, y+1)
		g.discover(x-1, y-1)
		g.discover(x-1, y+1)
		g.discover(x+1, y-1)
	} else {
		nearby, err = g.getNearby(x+1, y)
		if err == nil && nearby == 0 {
			g.discover(x+1, y)
		}
		nearby, err = g.getNearby(x-1, y)
		if err == nil && nearby == 0 {
			g.discover(x-1, y)
		}
		nearby, err = g.getNearby(x, y+1)
		if err == nil && nearby == 0 {
			g.discover(x, y+1)
		}
		nearby, err = g.getNearby(x, y-1)
		if err == nil && nearby == 0 {
			g.discover(x, y-1)
		}
		nearby, err = g.getNearby(x+1, y+1)
		if err == nil && nearby == 0 {
			g.discover(x+1, y+1)
		}
		nearby, err = g.getNearby(x-1, y-1)
		if err == nil && nearby == 0 {
			g.discover(x-1, y-1)
		}
		nearby, err = g.getNearby(x-1, y+1)
		if err == nil && nearby == 0 {
			g.discover(x-1, y+1)
		}
		nearby, err = g.getNearby(x+1, y-1)
		if err == nil && nearby == 0 {
			g.discover(x+1, y-1)
		}
	}

	return nil
}

func (g *game) setFlag(x, y int64) error {
	if x < 0 || y < 0 || x >= g.width || y >= g.height {
		return errors.New("no such position")
	}
	switch g.progress[y][x] {
	case PROGRESS_UNKNOWN:
		g.progress[y][x] = PROGRESS_FLAG
		g.flags--
	case PROGRESS_FLAG:
		g.progress[y][x] = PROGRESS_UNKNOWN
		g.flags++
	case PROGRESS_DISCOVERED:
		return errors.New("can't set a flag in a discovered area")
	default:
		return errors.New("unexpected progress")
	}
	return nil
}

func (g *game) judgeWin() error {
	if g.remaining == g.mines {
		g.status = STATUS_WON
	}
	return nil
}

func (g *game) getNearby(x, y int64) (int, error) {
	if x < 0 || y < 0 || x >= g.width || y >= g.height {
		return -1, errors.New("no such position")
	}

	if g.board[y][x] == MINE {
		return -1, errors.New("can't get a mine's nearby")
	}

	nearby := 0

	if x > 0 {
		if g.board[y][x-1] == MINE {
			nearby++
		}
		if y > 0 {
			if g.board[y-1][x-1] == MINE {
				nearby++
			}
		}
		if y < g.height-1 {
			if g.board[y+1][x-1] == MINE {
				nearby++
			}
		}
	}
	if x < g.width-1 {
		if g.board[y][x+1] == MINE {
			nearby++
		}
		if y > 0 {
			if g.board[y-1][x+1] == MINE {
				nearby++
			}
		}
		if y < g.height-1 {
			if g.board[y+1][x+1] == MINE {
				nearby++
			}
		}
	}
	if y > 0 {
		if g.board[y-1][x] == MINE {
			nearby++
		}
	}
	if y < g.height-1 {
		if g.board[y+1][x] == MINE {
			nearby++
		}
	}
	return nearby, nil
}
